package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			if (text.length() > width)
				System.out.println("textstørreennwidth");
			else if ((text.length() % 2) != (width % 2))
				System.out.println("ikkesentrerbar");
			else
				System.out.println("_".repeat(extra) + text + "_".repeat(extra));
			
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int right = (width - text.length());
			System.out.println(text + "_".repeat(right));
			return text + " ".repeat(right);
		}

		public String flushLeft(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
//	@Test
//	void test() {
//		fail("Not yet implemented");
//	}

	@Test
	void testCenter() {
//		assertEquals("  A  ", aligner.center("A", 5));
//		assertEquals(" foo ", aligner.center("foo", 5));
//		assertEquals(" foo ", aligner.center("foo", 5));
//		assertEquals(" foo ", aligner.center("A", 4));
//		assertEquals(" foo ", aligner.center("AA", 4));
		assertEquals(" foo ", aligner.center("AAAAAA", 4));
	}
	
//	@Test
//	void testRight() {
//		assertEquals("  A  ", aligner.flushRight("A", 5));
//		assertEquals(" foo ", aligner.flushRight("foo", 5));
//
//	}
//	
//	@Test
//	void testLeft() {
//		assertEquals("  A  ", aligner.flushLeft("A", 5));
//		assertEquals(" foo ", aligner.flushLeft("foo", 5));
//
//	}
}
